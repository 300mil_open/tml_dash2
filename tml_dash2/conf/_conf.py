import os
import re
import shutil
import sqlite3
from urllib.request import urlretrieve
from sqlalchemy import create_engine
from IPython.display import HTML


class ConfInit:
    r"""Initial configuration for the Visualization Notebook


    Example
    -------
    import tml_dash2 as tmld2
    conf = ConfInit(install_font=True)
    con = conf.pg_con(user='postgres', password='postgres', host='localhost', port=5432, database='postgres')
    con = conf.sqlite_con(db='./anyfile.sqlite')

    """

    def __init__(self, install_font=True):
        r"""Initiate a Conf object.

         Parameters
         ----------
         install_font : Option, bool
             If true installs Montserrat Google Font in the computer. Ubuntu only, default: True.

         Returns
         -------
         SQLite3 connection
             SQLite connection object.

         """

        if install_font:
            self.getFont()

        self.hydeCode = HTML('''<style>
        .output{
            position:relative;
        }
        .tml-hide{
            font-size:20px;
            font-weight:bold;
            position:fixed;
            z-index:999999999;
            left:20px;
            top:200px;
            color: black;
            font-size:42px;
        }
        .tml-hide a{
            text-decoration:none;
            color:gray;
        }
        .tml-hide a:hover{
            color:black;
        }
        </style>
        <script>
            code_show=true; 
            function code_toggle() {
             if (code_show){
             $('div.input').hide();
             } else {
             $('div.input').show();
             }
             code_show = !code_show
            } 
            $( document ).ready(code_toggle);
        </script>
        <div class="tml-hide">
        <a href="javascript:code_toggle()">&#X2699</a></div>
        ''')


    def sqlite_con(self, db):
        r"""Generates an Sqlite connection.

         Parameters
         ----------
         db : File path, str
             SQLite file path.

         Returns
         -------
         SQLite3 connection
             SQLite connection object.

         """

        con = sqlite3.connect(db)
        return con


    def pg_con(self, user='postgres', password='postgres', host='localhost', port=5432, database='postgres'):
        r"""Generates a PostgreSQL connection.

         Parameters
         ----------
         user : Username, str
             Postgres username, default: postgres.
         password : Password, str
             Postgres password, default: postgres.
         host : Database direction, str
             Postgres hostname, default: localhost.
         port : Database port, int
             Postgres database port, default: 5432.
         database : Database name, str
             Postgres database, default: postgres.

         Returns
         -------
         SQLEngine connection
             PostgreSQL connection object.

         """

        conf = {
            'user': user,
            'password': password,
            'host': host,
            'port': port,
            'database': database
        }
        con = create_engine(
            f"postgresql://{conf['user']}:{conf['password']}@{conf['host']}:{conf['port']}/{conf['database']}")
        return con

    def getFont(self, url='https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap'):
        r"""Downloads and install Montserrat Google Font in the system.

        Parameters
        ----------
        url : URL, str
            Google fonts retrieval URL generated in the Google Fonts interface.

        """

        folder_out = './temp/'

        for dirpath, dirnames, filenames in os.walk("/home/"):
            if re.match(r"/home/.*/share/fonts", dirpath):
                font_folder = dirpath

        if os.path.isdir(folder_out) is False:
            os.mkdir(folder_out)

        dst = f'{folder_out}_temp.css'
        urlretrieve(url, dst)

        f = open(dst)
        f = f.read()
        furl = re.findall(r"src: url\(([^\(\)]+)\)", f)
        ffamily = re.findall(r"font-family: \'(.*)\'\;", f)
        fstyle = re.findall(r"font-style: (.*)\;", f)
        fweight = re.findall(r"font-weight: (.*)\;", f)

        for fu, fa, fs, fw in zip(furl, ffamily, fstyle, fweight):
            font = f'{fa.lower()}_{fs}_{fw}.ttf'
            urlretrieve(fu, f'{folder_out}{font}')
            shutil.copyfile(f'{folder_out}{font}', f'{font_folder}{font}')

        shutil.rmtree(folder_out)

