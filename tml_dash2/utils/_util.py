import pandas as pd
import os
import numpy as np
import tml_pandas as tmpd
import matplotlib.colors as mplc
from matplotlib.colors import LinearSegmentedColormap
from SecretColors import Palette, utils


def makeColors(colors, n, invert=False):
    r"""Generates a gradient colorscale of n colors from a list of HEX colors.

     Parameters
     ----------
     colors : Color list, list
         List of ordered string like HEX colors.
     n : Step number, int
         Number os color steps for the gradient.
     invert : List configuration, bool
         If True returns an inverted list. Default: False.

     Returns
     -------
     Colors list
         List of ordered colors in HEX format.

     """

    if type(n) is list:
        n = len(n)

    if len(colors) == 1:
        cmap_list = colors * n

    else:
        cmap_list = []

        cm = LinearSegmentedColormap.from_list('vis', colors, N=n)

        for i in range(cm.N):
            rgba = cm(i)
            cmap_list.append(mplc.rgb2hex(rgba))

    if invert:
        cmap_list.reverse()

    return cmap_list



def makeColorScale(df, col, colors="#000000", color_by = "color_col", distribution=None, bins=None, invert=False, saturation=False):
    r"""Generates a Pandas DataFrame from a given DataFrame column binned and their respective colors.

     Parameters
     ----------
     df : DataFrame, panda's dataframe
         Panda's dataframe to be transformed.
     col : Colum name, str
         Panda's dataframe column name to be distributed by colors.
     colors : Colors, str or list
         Single color or list of colors in string format. If None is passed uses the Vis object defined colorscale.
     color_by : Column name, str
         Panda's dataframe column name to be used as a base for the color distribution.
     color_distribution : Distribution type, str
         Type of data distribution algorythm (single, category, jenks, natural_breaks, quantiles, percentiles, custom)
     bins : Number of bins or list of custom bins, int or list
         Number of bins to distribute the column with or list of percentiles to use as binning limits or list of limits for the customized binning..
     invert : List configuration, bool
         If True returns an inverted list. Default: False.
     saturation : Colors configuration, bool
         If True returns a gradient of saturation from the given color. Default: False.

     Returns
     -------
     Pandas dataFrame
         DataFrame with the column values distribution and their respective colors in a colorscale.

     """

    colors = [colors] if type(colors) is str else colors

    if type(bins) is int and distribution in ['percentiles', 'custom']:
        print('Bins must be a list of values')
        return df

    if len(colors) == 1:
        df['color_col'] = colors[0]
        return df

    elif distribution is None and bins is None:
        clrscl = makeColors(colors, df.shape[0], invert=invert)
        df['color_col'] = clrscl
        return df

    elif distribution is None and bins > 0:
        clrscl = makeColors(colors, bins, invert=invert)
        clrscl = [clrscl[i % len(clrscl)] for i in list(range(df.shape[0]))]
        df['color_col'] = clrscl
        return df

    elif distribution == 'category':
        df[col] = df[col].astype(str)
        bins = len(colors) if bins is None else bins
        clrscl = makeColors(colors, bins, invert=invert)
        cats = df[col].unique().tolist()
        color_cat = {ct: clrscl[cats.index(ct) % len(clrscl)] for ct in cats}
        df['color_col'] = df[col].apply(lambda x: color_cat[x])

        if saturation and color_by == "group_col":
            for g in df["group_col"].unique().tolist():
                c = df[df["group_col"] == g]["color_col"].values[0]
                cc = utils.hex_to_rgb(c)
                cc = utils.rgb_to_rgb255(max(cc), max(cc), max(cc))
                cc = utils.rgb255_to_hex(cc[0], cc[1], cc[2])
                clrs = [cc, c]
                clrs = makeColors(clrs, df["cat_col"].unique().shape[0] + 1)
                clrs = clrs[1:]
                df.loc[df["group_col"] == g, "color_col"] = clrs

        return df

    df[col] = df[col].astype('float64')

    dist = tmpd.Distribution(df[[col]].dropna(subset=[col]))

    if distribution == 'jenks':
        dist = dist.jenks(col, bins)
    elif distribution == 'natural_breaks':
        dist = dist.natural_breaks(col, bins)
    elif distribution == 'quantiles':
        dist = dist.quantiles(col, bins)
    elif distribution == 'percentiles':
        dist = dist.percentiles(col, bins)
    elif distribution == 'custom':
        dist = dist.custom(col, bins)

    inds = np.digitize(df[col], dist['bins'])
    clrscl = makeColors(colors, bins, invert=invert)
    clrscl = [clrscl[i] if i < len(clrscl) else clrscl[-1] for i in inds]
    df['color_col'] = clrscl

    if saturation and color_by == "group_col":
        for g in df["group_col"].unique().tolist():
            c = df[df["group_col"] == g]["color_col"].values[0]
            cc = utils.hex_to_rgb(c)
            cc = utils.rgb_to_rgb255(max(cc), max(cc), max(cc))
            cc = utils.rgb255_to_hex(cc[0], cc[1], cc[2])
            clrs = [cc, c]
            clrs = makeColors(clrs, df["cat_col"].unique().shape[0] + 1)
            clrs = clrs[1:]
            df.loc[df["group_col"] == g, "color_col"] = clrs

    return df



def getPlotlyKwarg(kw, val):
    r"""Translates a TML_DASH2 Kwarg into a Plotly arg.

     Parameters
     ----------
     kw : Kwarg, str
        Key word argument from TML_DASH.
     val : Kwarg value
        Any possible value for a Plotly configuration.

     Returns
     -------
     Kwarg dictionary
         Plotly argument like dictionary.

     """

    if kw == "lo_width":
        kw = {"width": val}
    elif kw == "lo_height":
        kw = {"height": val}
    elif kw == "lo_bgcolor":
        kw = {"paper_bgcolor": val}
    elif kw == "lo_plot_bgcolor":
        kw = {"plot_bgcolor": val}
    elif kw == "lo_margin_t":
        kw = {"margin": {"t": val}}
    elif kw == "lo_margin_b":
        kw = {"margin": {"b": val}}
    elif kw == "lo_margin_l":
        kw = {"margin": {"l": val}}
    elif kw == "lo_margin_r":
        kw = {"margin": {"r": val}}
    elif kw == "lo_title_text":
        kw = {"title": {"text": val}}
    elif kw == "lo_title_xref":
        kw = {"title": {"xref": val}}
    elif kw == "lo_title_x":
        kw = {"title": {"x": val}}
    elif kw == "lo_title_font_family":
        kw = {"title": {"font": {"family": val}}}
    elif kw == "lo_title_font_size":
        kw = {"title": {"font": {"size": val}}}
    elif kw == "lo_title_font_color":
        kw = {"title": {"font": {"color": val}}}
    elif kw == "lo_legend_visible":
        kw = {"showlegend": val}
    elif kw == "lo_legend_bgcolor":
        kw = {"legend": {"bgcolor": val}}
    elif kw == "lo_legend_x":
        kw = {"legend": {"x": val}}
    elif kw == "lo_legend_y":
        kw = {"legend": {"y": val}}
    elif kw == "lo_legend_tracegroupgap":
        kw = {"legend": {"tracegroupgap": val}}
    elif kw == "lo_legend_xanchor":
        kw = {"legend": {"xanchor": val}}
    elif kw == "lo_legend_yanchor":
        kw = {"legend": {"yanchor": val}}
    elif kw == "lo_legend_bordercolor":
        kw = {"legend": {"bordercolor": val}}
    elif kw == "lo_legend_borderwidth":
        kw = {"legend": {"borderwidth": val}}
    elif kw == "lo_legend_font_family":
        kw = {"legend": {"font": {"family": val}}}
    elif kw == "lo_legend_font_size":
        kw = {"legend": {"font": {"size": val}}}
    elif kw == "lo_legend_font_color":
        kw = {"legend": {"font": {"color": val}}}
    elif kw == "x_visible":
        kw = {"visible": val}
    elif kw == "y_visible":
        kw = {"visible": val}
    else:
        kw = None

    return kw


def defaultVals(**kwargs):
    r"""Generates a dictionary to translate the kwargs into PLotly Figure kwargs.

     Parameters
     ----------
     **kwargs : Kwarg dictionary, dict
        Check all for kwargs

     Returns
     -------
     Kwarg dictionary
         Dictionary with the Default values for the charts.

     """

    kwargDict = {
        'agg': 'value', # Aggregation operation for charts that aggregte (value, sum, mean, med, min, max)
        'stack_order': 'group_col', # Shift between the cat_col and group_col to which will lead grouped Grouped bars and Stacked bars
        'color_by': 'color_col', # Defines which column will be used to lead the color distribution
        'colors': None, # List of colors
        'color_saturation': None, # If True the effect of saturation in colors for grouped bars will be applied
        'color_distribution': None, # Which color distribution from TML_PANDAS
        'bins': None, # Number of bins for the color distribution
        'invert_colors': False, # If true reverse the color list
        'sort_by': 'sort_col', # Defines which column will beused to order the charts
        'sort_asc': True, # If True ascendant order else descendant
        'auto_range_x': False, # If True autoadjust the x range for horizontal charts
        'auto_range_y': False, # If True autoadjust the y range for vertical charts
        'img_name': 'test', # The name in which the chart image will be saved under
        'size': 50, # Aproximately size in PX for each bar, the final chart size will be based on it
        'perc_val': False, # If True the value to be ploted is a percent from the total
        'text_threshold': None, # A threshold for label visibility in donut charts
        'group_threshold': False, # If True groups values below the threshold into a larger group
        'crs': 4326, # CRS projection for the Map
        'donut_sort': True, # If True sorts the donut chart based on the order that the DF is, else it is sorte in ascending order
        'hole_size': 0.5, # The hole size for the Donut in proportion to the final size
        'hole_lbl': None, # The center label for the donut chart
        'hole_lbl_font_family': 'Montserrat', # Font family for the center label
        'hole_lbl_font_size': 12, # Font size for the center label
        'hole_lbl_font_color': '#000000', # Font color for the center label
        'waffle_size': 25, # Waffle cell size in PX
        'waffle_sample': 1, # Size of the sample to be represented in the waffle
        'lbl_show': False, # If True shows the labels inside the plots for each category
        'lbl_font_size': 10, # Font size for labels
        'lbl_xshift': 0, # Horizontal displacement for labels in PX
        'lbl_yshift': 0, # Vertival displacement for labels in PX
        'lo_width': 500, # Chart width. Automatic for vertical bar charts
        'lo_height': 500, # Chart height. Automatic for horizontal bar charts
        'lo_bgcolor': '#ffffff', # Chart background color around axis
        'lo_plot_bgcolor': '#ffffff', # Chart background color
        'lo_margin_t': 0, # Defines the upper margin of the chart
        'lo_margin_b': 0, # Defines the lower margin of the chart
        'lo_margin_l': 0, # Defines the left margin of the chart
        'lo_margin_r': 0, # Defines the right margin of the chart
        'lo_title_text': '', # Defines the title of the chart. Must add a TOP MARGIN to fit the title
        'lo_title_xref': 'paper', # Defines the reference where the title is placed
        'lo_title_x': 0, # Defines an horizontal displacement for the title
        'lo_title_font_family': 'Montserrat', # Font family for the title
        'lo_title_font_size': 10, # Font size for the title
        'lo_title_font_color': '#000000', # Font color for the title
        'lo_legend_visible': False, # If True shows the legend
        'lo_legend_bgcolor': '#ffffff', # Legend background color
        'lo_legend_x': 0, # Legend horizontal displacement
        'lo_legend_y': 0, # Legend vertical displacement
        'lo_legend_xanchor': 'left', # Legend horizontal anchor
        'lo_legend_yanchor': 'top', # Legend vertical anchor
        'lo_legend_bordercolor': '#ffffff', # Legend border color
        'lo_legend_borderwidth': 0, # Legend border width
        'lo_legend_font_family': 'Montserrat', # Font family for the legend
        'lo_legend_font_size': 10, # Font family for the legend
        'lo_legend_font_color': '#000000',# Font family for the legend
        'lo_legend_tracegroupgap': 10, # Controls the space between items of the legend IT DOESNT WORK
        'x_visible': False, # If True labels for the X axis are visible
        'x_label_xshift': 0, # Horizontal displacement for labels in the X axis
        'x_label_yshift': 0, # Vertical displacement for labels in the X axis
        'y_visible': False, # If True labels for the Y axis are visible
        'y_label_xshift': 0, # Horizontal displacement for labels in the Y axis
        'y_label_yshift': 0, # Vertical displacement for labels in the Y axis
    }

    kwargDict.update(kwargs)

    return kwargDict



def updateKwargs(kwargDict, **kwargs):
    r"""Copies a Kwarg dictionary and updates its values.

     Parameters
     ----------
     kwargDict : Kwarg dictionary, dict
         Dictionary containing kwargs and values to be updated.
     **kwargs : Kwarg dictionary, dict
        Check all for kwargs

     Returns
     -------
     Kwarg dictionary
         Dictionary with updated values for the charts.

     """

    updtKwargDict = kwargDict.copy()
    updtKwargDict.update(kwargs)

    return updtKwargDict


def make_legend(legend):
    r"""Generates a Dict object for label translations.

    Parameters
    ----------
    legend : Excel file path or dict, str or dict
        File or dict with labels and their correspondences to be used in legends.

    Returns
    -------
    Dictionary
        Dictionary with labels and their correspondences.

    """

    if legend is None:
        print('Legend dictionary not specified')
        return
    elif type(legend) is dict:
        legend = legend
    elif os.path.isfile(legend):
        legend = {x['cat']: x['label'] for x in pd.read_excel(legend).to_dict(orient='records')}

    return legend


def updateLegendSize(self, number_legend_items):
    r"""Evaluates and updates the chart height based on the number of items in the legend.

    Parameters
    ----------
    number_legend_items : Number of items, int
        Number of items in the legend.

    """

    number_legend_items = number_legend_items if number_legend_items <= 30 else 30

    original_height = self.vl.fig.layout['height']
    llegend_font_size = self.vl.fig.layout['legend']['font']['size']
    item_size = 5.5 + ((llegend_font_size - 1) * 0.5)
    legend_box_size = (item_size * number_legend_items) + 50 + 30

    new_height = original_height + legend_box_size

    self.vl.update(lo_height=new_height, lo_margin_b=legend_box_size, lo_legend_y=-(15.0 / original_height))


def makeCatChartDf(df, val_col, color_col, sort_col, cat_col, group_col, **kwargs):
    r"""Formats the given DataFrame into a new one to be used for categorical charts.

    Parameters
    ----------
    df : DataFrame, panda's dataframe
        Panda's dataframe to be transformed.
    val_col : Column name or list of column names, str
        Column name with numerical values or list of columns with numerical values,  if None is passed the val_col value will be 1.
    color_col : Column name, str
        Column name with numerical or categorical values.
    sort_col : Column name, str
        Column name with numerical or categorical values.
    cat_col : Column name, str
        Column name with categorical values,  if None is passed the val_col column name's are used as categories.
    group_col : Column name, str
        Column name with categorical values,  if None is passed the cat_col column is used as categories.
    **kwargs : kwargs
        Plotly and custom keywargs.

    Returns
    -------
    Pandas DataFrame
        Formated panda's dataframe to use in the categorical charts.

    """

    aggDict = {}

    dfTemp = df.copy()
    dfTemp['idx_col'] = dfTemp.index

    if type(val_col) is list:
        dfTemp = pd.melt(
            dfTemp,
            id_vars=[c for c in dfTemp.columns if c not in val_col],
            value_vars=val_col,
            var_name='cat_col',
            value_name='val_col',
            ignore_index=False
        )

        cat_col = 'cat_col'

    elif val_col in dfTemp.columns:
        dfTemp['val_col'] = dfTemp[val_col]

    elif val_col is None:
        dfTemp['val_col'] = 1

    if cat_col is None and val_col is None:
        dfTemp['cat_col'] = 'Total'
    elif cat_col is None:
        dfTemp['cat_col'] = val_col
    elif cat_col in dfTemp.columns:
        dfTemp['cat_col'] = dfTemp[cat_col]

    if group_col in dfTemp.columns:
        dfTemp['group_col'] = dfTemp[group_col]
    else:
        dfTemp['group_col'] = dfTemp['cat_col']

    color_col = 'val_col' if (color_col is None and color_by == 'color_col') else color_col
    dfTemp['color_col'] = dfTemp[color_col]
    sort_col = 'val_col' if (sort_col is None and sort_by == 'sort_col') else sort_col
    dfTemp['sort_col'] = dfTemp[sort_col] if type(sort_col) is str else sort_col[0]

    cols = ['idx_col', 'cat_col', 'val_col', 'group_col', 'color_col', 'sort_col']

    if 'geom_col' in kwargs:
        aggDict['geom_col'] = 'first'
        dfTemp['geom_col'] = dfTemp[kwargs["geom_col"]]
        dfTemp['geom_col'] = dfTemp['geom_col'].astype(str)

        cols.append('geom_col')

    dfTemp = dfTemp[cols].copy()

    dfTemp['cat_col'] = dfTemp['cat_col'].astype(str)
    dfTemp['group_col'] = dfTemp['group_col'].astype(str)

    if color_col in ['cat_col', 'group_col'] or kwargs["agg"] == 'value':
        aggDict['color_col'] = 'first'
        aggDict['sort_col'] = 'first'
    else:
        aggDict['color_col'] = 'first'
        aggDict['sort_col'] = 'first'

    if kwargs["agg"] == 'value':
        aggDict['val_col'] = 'first'
    else:
        aggDict['val_col'] = kwargs["agg"]

    group = ['group_col', 'cat_col']

    dfTemp = dfTemp.groupby(group).agg(aggDict).reset_index()
    dfTemp.dropna(inplace=True)

    return dfTemp


def prepareDF(self, df, val_col, color_col, sort_col, cat_col, group_col, **kwargs):
    r"""Prepares the given DataFrame to build charts.

    Parameters
    ----------
    df : DataFrame, panda's dataframe
        Panda's dataframe to be transformed.
    val_col : Column name or list of column names, str
        Column name with numerical values or list of columns with numerical values.
    color_col : Column name, str
        Column name with numerical or categorical values.
    sort_col : Column name, str
        Column name with numerical or categorical values.
    cat_col : Column name, str
        Column name with categorical values,  if None is passed the val_col column name's are used as categories.
    group_col : Column name, str
        Column name with categorical values,  if None is passed the cat_col column is used as categories.
    **kwargs : kwargs
        Plotly and custom keywargs.

    Returns
    -------
    Pandas DataFrame
        Formated panda's dataframe to plot.
    """

    colors = kwargs["colors"] if kwargs["colors"] else self.colors
    color_by = kwargs["color_by"]
    sort_by = kwargs["sort_by"]

    color_col = 'val_col' if (color_col is None and color_by == 'color_col') else color_col if color_col else color_by
    sort_col = 'val_col' if (sort_col is None and sort_by == 'sort_col') else sort_col if sort_col else sort_by

    dfChart = makeCatChartDf(df=df, val_col=val_col, color_col=color_col, sort_col=sort_col, cat_col=cat_col, group_col=group_col, **kwargs)

    if kwargs["stack_order"] == 'cat_col':
        dfChart.rename(columns={'cat_col': 'group_col', 'group_col': 'cat_col'}, inplace=True)

    if kwargs["perc_val"]:
        dfTot = dfChart.groupby('cat_col')['val_col'].sum().reset_index()
        dfChart['val_abs_col'] = dfChart['val_col']
        dfChart['val_col'] = dfChart.apply(
            lambda row: (row['val_col'] / dfTot[dfTot['cat_col'] == row['cat_col']]['val_col'].values[0]) * 100,
            axis=1)

    if kwargs["text_threshold"]:
        dfChart['cat_col'] = dfChart.apply(
            lambda row: row['cat_col'] if (row['val_col'] / dfChart['val_col'].sum() * 100) >= kwargs[
                "text_threshold"] else 'Otros', axis=1)
        dfChart['group_col'] = dfChart.apply(
            lambda row: row['cat_col'] if (row['val_col'] / dfChart['val_col'].sum() * 100) >= kwargs[
                "text_threshold"] else 'Otros', axis=1)

        if kwargs["color_by"] == 'cat_col':
            dfChart.drop_duplicates(inplace=True)
        else:
            dfChart = dfChart.groupby(['group_col', 'cat_col'])[
                ['val_col', 'color_col', 'sort_col']].sum().reset_index()

    if kwargs["color_saturation"]:
        clrs = colors
        sat = True
    elif kwargs["color_distribution"] == 'single':
        clrs = self.color_single
        sat = False
    else:
        clrs = colors
        sat = False

    dfChart = makeColorScale(dfChart, col='color_col', colors=clrs, color_by=color_by,
                             distribution=kwargs["color_distribution"], bins=kwargs["bins"],
                             invert=kwargs["invert_colors"], saturation=sat)

    if kwargs["text_threshold"]:
        dfChart['color_col'] = dfChart.apply(
            lambda row: row['color_col'] if row['cat_col'] != 'Otros' else colors[0], axis=1)

    if kwargs["sort_by"]:
        dfChart.sort_values(by=kwargs["sort_by"], ascending=kwargs["sort_asc"], inplace=True)

    return dfChart