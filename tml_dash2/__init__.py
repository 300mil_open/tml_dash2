from .conf._conf import ConfInit
from .vis._vis import Vis
import pandas as pd


__all__ = ['ConfInit', 'Vis']
__version__ = '1.4.7'
__author__ = 'Andre Resende'


txt = r"""
    Transversal module to generate Plotly Charts from Pandas DataFramse
        ConfInit
        VisLayout
        Vis
"""

if __doc__ is None:
    __doc__ = txt
else:
    __doc__ = txt + __doc__