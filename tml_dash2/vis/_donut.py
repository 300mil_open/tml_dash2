import tml_dataclean as tmdc
import plotly.graph_objects as go
import re
from tml_dash2.utils._util import *


def donut(
    self,
    df,
    val_col,
    cat_col = None,
    color_col = None,
    sort_col = None,
    **kwargs
):
    r"""Pie/Donut chart.

    Parameters
    ----------
    df : DataFrame, panda's dataframe
        Panda's dataframe to be transformed.
    val_col : Column name or list of column names, str
        Column name with numerical values or list of columns with numerical values.
    cat_col : Column name, str
        Column name with categorical values,  if None is passed the val_col column name's are used as categories.
    color_col : Column name, str
        Column name with numerical or categorical values, If None is passed the val_col column is used as color_col.
    sort_col : Column name, str
        Column name with numerical or categorical values, If None is passed the val_col column is used as sort_col.
    **kwargs : kwargs
        Plotly and custom keywargs.

    """

    self.vl.reset()

    chartConf = updateKwargs(self.defaultKwargs, **kwargs)
    chartConf["perc_val"] = False
    chartConf["stack_order"] = None
    chartConf["group_threshold"] = chartConf["group_threshold"] if chartConf["text_threshold"] else False
    #         print(chartConf)

    dfChart = prepareDF(self, df=df, val_col=val_col, color_col=color_col, sort_col=sort_col,
                             cat_col=cat_col, group_col=None, **chartConf)

    if self.legend:
        dfChart['cat_col'] = dfChart['cat_col'].apply(lambda x: self.legend[x] if x in self.legend else x)

    t = self.vl.fig.layout['margin']['t']
    b = self.vl.fig.layout['margin']['b']
    t = t if 'lo_margin_t' not in kwargs else kwargs['lo_margin_t']
    b = b if 'lo_margin_b' not in kwargs else kwargs['lo_margin_b']
    h = self.vl.fig.layout['height'] - t - b

    self.vl.update(**kwargs)

    if self.vl.fig.layout['showlegend']:
        updateLegendSize(self, dfChart['cat_col'].unique().shape[0])

    if chartConf["sort_by"]:
        dfChart.sort_values(by=chartConf["sort_by"], ascending=chartConf["sort_asc"], inplace=True)

    def makeTextCol(df, value, text_threshold):
        if text_threshold and (value / df['val_col'].sum() * 100 < text_threshold):
            txt = ''
        else:
            txt = f"{str(round(value / df['val_col'].sum() * 100, 1))}%"
        return txt

    def makeLblCol(row):
        txt = f"{row['cat_col']} ({tmdc.clean_decimal(float(row['val_col']), get_number=False)})"

        if re.match(r"^.*,0\)$", txt):
            txt = txt.replace(',0)', ')')

        return txt

    dfChart['text_col'] = dfChart.apply(
        lambda row: makeTextCol(dfChart, row['val_col'], chartConf["text_threshold"]), axis=1)
    dfChart['lbl_col'] = dfChart.apply(lambda row: makeLblCol(row), axis=1)

    trace = go.Pie(
        labels=dfChart['lbl_col'],
        values=dfChart['val_col'],
        text=dfChart['text_col'],
        marker=dict(colors=dfChart['color_col']),
        hole=chartConf["hole_size"],
        hoverinfo="label+percent",
        textinfo="text",
        textposition="inside",
        direction="clockwise",
        sort=chartConf["donut_sort"]
    )

    if chartConf["lbl_show"]:
        trace['text'] = [round(x, 2) for x in dfChart['val_col']]
        trace['insidetextfont'] = {'size': chartConf["lbl_font_size"], 'family': 'Montserrat'}
        trace['textposition'] = 'inside'

    if chartConf["hole_lbl"]:
        self.vl.fig.add_annotation(
            width=self.vl.fig.layout['width'] * (chartConf["hole_size"] / 2),
            font={'size': chartConf["hole_lbl_font_size"], 'family': chartConf["hole_lbl_font_family"],
                  'color': chartConf["hole_lbl_font_color"]},
            x=0,
            xanchor='center',
            y=0,
            yanchor='middle',
            xref="x",
            yref="y",
            yshift=0,
            text=chartConf["hole_lbl"],
            showarrow=False,
            ax=0,
            ay=0
        )

    self.vl.fig.add_trace(trace)

    self.output(self.vl.fig, chartConf["img_name"])