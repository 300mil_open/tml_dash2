import plotly.graph_objects as go
from tml_dash2.utils._util import *


def vbar_group(
        self,
        df,
        val_col,
        cat_col=None,
        group_col=None,
        color_col=None,
        sort_col=None,
        **kwargs
):
    r"""Absolute vertical grouped bar chart.

    Parameters
    ----------
    df : DataFrame, panda's dataframe
        Panda's dataframe to be transformed.
    val_col : Column name or list of column names, str
        Column name with numerical values or list of columns with numerical values.
    cat_col : Column name, str
        Column name with categorical values,  if None is passed the val_col column name's are used as categories.
    group_col : Column name, str
        Column name with categorical values,  if None is passed the cat_col column is used as categories.
    color_col : Column name, str
        Column name with numerical or categorical values, If None is passed the val_col column is used as color_col.
    sort_col : Column name, str
        Column name with numerical or categorical values, If None is passed the val_col column is used as sort_col.
    **kwargs : kwargs
        Plotly and custom keywargs.

    """

    self.vl.reset()

    chartConf = updateKwargs(self.defaultKwargs, **kwargs)
    chartConf["perc_val"] = False
    chartConf["text_threshold"] = None
    #         print(chartConf)

    dfChart = prepareDF(self, df=df, val_col=val_col, color_col=color_col, sort_col=sort_col,
                             cat_col=cat_col, group_col=group_col, **chartConf)

    n = dfChart['cat_col'].shape[0]
    l = self.vl.fig.layout['margin']['l']
    r = self.vl.fig.layout['margin']['r']
    l = l if 'lo_margin_l' not in kwargs else kwargs['lo_margin_l']
    r = r if 'lo_margin_r' not in kwargs else kwargs['lo_margin_r']

    self.vl.update(lo_width=(n * chartConf["size"]) + l + r, **kwargs)

    if self.vl.fig.layout['showlegend']:
        dfLegend = dfChart[['cat_col', 'color_col']].copy().drop_duplicates()
        updateLegendSize(self, dfLegend.shape[0])

        for _, row in dfLegend.iterrows():
            label = self.legend[group] if (self.legend and row['cat_col'] in self.legend) else row['cat_col']

            trace = go.Bar(
                y=[0],
                x=[0],
                width=[0],
                orientation='v',
                showlegend=True,
                marker_color=row['color_col'],
                name=row['cat_col'],
                hoverinfo="skip"
            )

            self.vl.fig.add_trace(trace)

    x = 1
    for group in dfChart['group_col'].unique():
        dfTempGroup = dfChart[dfChart['group_col'] == group].copy()

        cats = dfTempGroup['cat_col'].unique().tolist()

        for i in list(range(len(cats))):
            category = cats[i]

            dfTempCat = dfTempGroup[dfTempGroup['cat_col'] == category].copy()

            trace = go.Bar(
                x=[x],
                y=dfTempCat['val_col'],
                offset=0,
                width=[0.75],
                orientation='v',
                marker_color=dfTempCat['color_col'],
                showlegend=False,
                name=f"{group}: {category}",
                hoverinfo="name+x"
            )

            if chartConf["lbl_show"]:
                h = self.vl.fig.layout['height'] - (
                        self.vl.fig.layout['margin']['t'] + self.vl.fig.layout['margin']['b'])
                y = dfTempCat["val_col"].values[0]
                txt = str(round(y, 2))

                threshold = h / dfChart["val_col"].max()

                if (y * threshold) <= ((chartConf["lbl_font_size"]) * len(txt)):
                    yanchor = 'bottom'
                    yshift = 2
                else:
                    yanchor = 'top'
                    yshift = -2

                self.vl.fig.add_annotation(
                    x=x,
                    y=y,
                    text=txt,
                    font={'size': chartConf["lbl_font_size"], 'family': 'Montserrat', 'color': '#000000'},
                    showarrow=False,
                    textangle=270,
                    xanchor='center',
                    yanchor=yanchor,
                    xshift=chartConf["size"] / 4,
                    yshift=yshift
                )

            self.vl.fig.add_trace(trace)

            x += 1

        label = self.legend[group] if (self.legend and group in self.legend) else group
        self.vl.fig.add_annotation(
            x=x - (len(cats) + 0.75),
            y=0,
            text=label,
            font={'size': chartConf["lbl_font_size"], 'family': 'Montserrat', 'color': '#000000'},
            showarrow=False,
            textangle=270,
            xanchor='left',
            yanchor='bottom',
            xshift=2
        )

        x += 1

    self.output(self.vl.fig, chartConf["img_name"])