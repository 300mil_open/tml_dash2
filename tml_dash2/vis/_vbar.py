import plotly.graph_objects as go
from tml_dash2.utils._util import *


def vbar(
        self,
        df,
        val_col,
        cat_col=None,
        color_col=None,
        sort_col=None,
        **kwargs
):
    r"""Vertical bar chart.

    Parameters
    ----------
    df : DataFrame, panda's dataframe
        Panda's dataframe to be transformed.
    val_col : Column name or list of column names, str
        Column name with numerical values or list of columns with numerical values.
    cat_col : Column name, str
        Column name with categorical values,  if None is passed the val_col column name's are used as categories.
    color_col : Column name, str
        Column name with numerical or categorical values, If None is passed the val_col column is used as color_col.
    sort_col : Column name, str
        Column name with numerical or categorical values, If None is passed the val_col column is used as sort_col.
    **kwargs : kwargs
        Plotly and custom keywargs.

    """

    self.vl.reset()

    chartConf = updateKwargs(self.defaultKwargs, **kwargs)
    chartConf["stack_order"] = None
    chartConf["perc_val"] = False
    chartConf["text_threshold"] = None
    #         print(chartConf)

    dfChart = prepareDF(self, df=df, val_col=val_col, color_col=color_col, sort_col=sort_col,
                             cat_col=cat_col, group_col=None, **chartConf)

    n = dfChart['cat_col'].shape[0]
    l = self.vl.fig.layout['margin']['l']
    r = self.vl.fig.layout['margin']['r']
    l = l if 'lo_margin_l' not in kwargs else kwargs['lo_margin_l']
    r = r if 'lo_margin_r' not in kwargs else kwargs['lo_margin_r']

    self.vl.update(lo_width=(n * chartConf["size"]) + l + r, **kwargs)

    if self.vl.fig.layout['showlegend']:
        updateLegendSize(self, 1)

    name = cat_col if cat_col else dfChart['cat_col'].values[0]

    trace = go.Bar(
        x=dfChart['cat_col'],
        y=dfChart['val_col'],
        width=0.5,
        offset=0.25,
        orientation='v',
        marker_color=dfChart['color_col'],
        name=name
    )

    self.vl.fig.add_trace(trace)

    for cat in dfChart['cat_col'].unique():
        label = self.legend[cat] if (self.legend and cat in self.legend) else cat
        self.vl.fig.add_annotation(
            x=cat,
            y=0,
            text=label,
            font={'size': chartConf["lbl_font_size"], 'family': 'Montserrat', 'color': '#000000'},
            showarrow=False,
            textangle=270,
            xanchor='right',
            yanchor='bottom',
            xshift=chartConf["lbl_font_size"]
        )

        if chartConf["lbl_show"]:
            h = self.vl.fig.layout['height'] - (
                    self.vl.fig.layout['margin']['t'] + self.vl.fig.layout['margin']['b'])
            y = dfChart[dfChart["cat_col"] == cat]["val_col"].values[0]
            txt = str(round(y, 2))

            threshold = h / dfChart["val_col"].max()

            if (y * threshold) <= ((chartConf["lbl_font_size"]) * len(txt)):
                yanchor = 'bottom'
                yshift = 2
            else:
                yanchor = 'top'
                yshift = -2

            self.vl.fig.add_annotation(
                x=cat,
                y=y,
                text=txt,
                font={'size': chartConf["lbl_font_size"], 'family': 'Montserrat', 'color': '#000000'},
                showarrow=False,
                textangle=270,
                xanchor='center',
                yanchor=yanchor,
                xshift=chartConf["size"] / 2,
                yshift=yshift
            )

    self.output(self.vl.fig, chartConf["img_name"])