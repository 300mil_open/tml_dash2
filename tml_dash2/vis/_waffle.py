import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.font_manager as fm
from pywaffle import Waffle
from tml_dash2.utils._util import *


def waffle(
        self,
        df,
        val_col = None,
        cat_col = None,
        color_col = None,
        sort_col = None,
        **kwargs
):
    r"""Waffle chart.

    Parameters
    ----------
    df : DataFrame, panda's dataframe
        Panda's dataframe to be transformed.
    val_col : Column name or list of column names, str
        Column name with numerical values or list of columns with numerical values.
    cat_col : Column name, str
        Column name with categorical values,  if None is passed the val_col column name's are used as categories.
    color_col : Column name, str
        Column name with numerical or categorical values, If None is passed the val_col column is used as color_col.
    sort_col : Column name, str
        Column name with numerical or categorical values, If None is passed the val_col column is used as sort_col.
    **kwargs : kwargs
        Plotly and custom keywargs.

    """

    dpi = 300
    plt.rcParams['figure.dpi'] = dpi
    px = 1 / plt.rcParams['figure.dpi']

    self.vl.reset()

    chartConf = updateKwargs(self.defaultKwargs, **kwargs)
    chartConf["perc_val"] = False
    chartConf["stack_order"] = None
    chartConf["text_threshold"] = None
    chartConf["agg"] = "sum" if "agg" not in kwargs else chartConf["agg"]
    #         print(chartConf)

    dfChart = prepareDF(self, df=df, val_col=val_col, color_col=color_col, sort_col=sort_col,
                             cat_col=cat_col, group_col=None, **chartConf)

    dfChart['val_label_col'] = dfChart['val_col']
    dfChart['val_col'] = dfChart['val_col'] * chartConf["waffle_sample"]

    w = self.vl.fig.layout['width']
    columns = int(w / chartConf["waffle_size"])
    rows = int(dfChart['val_col'].sum() / columns)
    h = rows * chartConf["waffle_size"]

    plt.rcParams['figure.figsize'] = w, h

    self.vl.update(lo_height=h, **kwargs)

    data = {row['cat_col']: row['val_col'] for _, row in dfChart.iterrows()}
    labels = [f"{row['cat_col']} ({row['val_label_col']})" for _, row in dfChart.iterrows()]
    colors = dfChart['color_col'].to_list()

    if self.vl.fig['layout']['showlegend']:
        handles = [mpatches.Patch(color=c, label=l) for c, l in zip(colors, labels)]
        font = fm.FontProperties(fname=self.vl.getMPLFont(),
                                 size=self.vl.fig['layout']['legend']['font']['size'] / 4)

        updateLegendSize(self, len(handles))

        h = self.vl.fig['layout']['height']
        w = self.vl.fig['layout']['width']

        plt.rcParams['figure.figsize'] = w, h

    fig = plt.figure(
        FigureClass=Waffle,
        columns=columns,
        rows=rows,
        figsize=(w * px, h * px),
        values=data,
        frameon=False,
        vertical=True,
        icons='circle',
        icon_size=chartConf["waffle_size"] / 6,
        icon_style='solid',
        colors=colors,
        facecolor='#FFFFFF',
        starting_location='NW',
        block_arranging_style='normal',
        interval_ratio_x=0,
        interval_ratio_y=0,
        tight=True,
        labels=labels
    )

    if self.vl.fig['layout']['showlegend']:
        plt.legend(handles=handles, loc='upper left', bbox_to_anchor=(0, (-50 / h)), frameon=False, prop=font)
    else:
        plt.legend('', frameon=False)

    if self.vl.fig['layout']['title_text'] != '':
        font = fm.FontProperties(fname=self.vl.getMPLFont(),
                                 size=self.vl.fig['layout']['title']['font']['size'] / 4)
        plt.title(label=self.vl.fig['layout']['title_text'], loc='left', fontproperties=font)

    plt.margins(0)
    self.output(fig, chartConf["img_name"], fig_type='matplotlib')