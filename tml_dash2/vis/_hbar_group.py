import plotly.graph_objects as go
from tml_dash2.utils._util import *


def hbar_group(
        self,
        df,
        val_col,
        cat_col=None,
        group_col=None,
        color_col=None,
        sort_col=None,
        **kwargs
):
    r"""Absolute horizontal grouped bar chart.

    Parameters
    ----------
    df : DataFrame, panda's dataframe
        Panda's dataframe to be transformed.
    val_col : Column name or list of column names, str
        Column name with numerical values or list of columns with numerical values.
    cat_col : Column name, str
        Column name with categorical values,  if None is passed the val_col column name's are used as categories.
    group_col : Column name, str
        Column name with categorical values,  if None is passed the cat_col column is used as categories.
    color_col : Column name, str
        Column name with numerical or categorical values, If None is passed the val_col column is used as color_col.
    sort_col : Column name, str
        Column name with numerical or categorical values, If None is passed the val_col column is used as sort_col.
    **kwargs : kwargs
        Plotly and custom keywargs.

    """

    self.vl.reset()

    chartConf = updateKwargs(self.defaultKwargs, **kwargs)
    chartConf["perc_val"] = False
    chartConf["text_threshold"] = None
    #         print(chartConf)

    dfChart = prepareDF(self, df=df, val_col=val_col, color_col=color_col, sort_col=sort_col,
                             cat_col=cat_col, group_col=group_col, **chartConf)

    n = dfChart.shape[0]
    t = self.vl.fig.layout['margin']['t']
    b = self.vl.fig.layout['margin']['b']
    t = t if 'lo_margin_t' not in kwargs else kwargs['lo_margin_t']
    b = b if 'lo_margin_b' not in kwargs else kwargs['lo_margin_b']

    self.vl.update(lo_height=(n * chartConf["size"]) + t + b, **kwargs)

    if self.vl.fig.layout['showlegend']:
        dfLegend = dfChart[['cat_col', 'color_col']].copy().drop_duplicates()
        updateLegendSize(self, dfLegend.shape[0])

        for _, row in dfLegend.iterrows():
            label = self.legend[group] if (self.legend and row['cat_col'] in self.legend) else row['cat_col']

            trace = go.Bar(
                y=[0],
                x=[0],
                width=[0],
                orientation='h',
                showlegend=True,
                marker_color=row['color_col'],
                name=row['cat_col'],
                hoverinfo="skip"
            )

            self.vl.fig.add_trace(trace)

    y = 1
    for group in dfChart['group_col'].unique():
        dfTempGroup = dfChart[dfChart['group_col'] == group].copy()

        cats = dfTempGroup['cat_col'].unique().tolist()

        for i in list(range(len(cats))):
            category = cats[i]

            dfTempCat = dfTempGroup[dfTempGroup['cat_col'] == category].copy()

            trace = go.Bar(
                y=[y],
                x=dfTempCat['val_col'],
                offset=0,
                width=[0.75],
                orientation='h',
                marker_color=dfTempCat['color_col'],
                showlegend=False,
                name=f"{group}: {category}",
                hoverinfo="name+x"
            )

            if chartConf["lbl_show"]:
                w = self.vl.fig.layout['width'] - (
                        self.vl.fig.layout['margin']['r'] + self.vl.fig.layout['margin']['l'])
                x = dfTempCat["val_col"].values[0]
                txt = str(round(x, 2))

                threshold = w / dfChart["val_col"].max()

                if (x * threshold) <= ((chartConf["lbl_font_size"]) * len(txt)):
                    xanchor = 'left'
                    xshift = 2
                else:
                    xanchor = 'right'
                    xshift = -2

                self.vl.fig.add_annotation(
                    x=x,
                    y=y,
                    text=txt,
                    font={'size': chartConf["lbl_font_size"], 'family': 'Montserrat', 'color': '#000000'},
                    showarrow=False,
                    textangle=0,
                    xanchor=xanchor,
                    yanchor='middle',
                    yshift=chartConf["size"] / 4,
                    xshift=xshift
                )

            self.vl.fig.add_trace(trace)

            y += 1

        label = self.legend[group] if (self.legend and group in self.legend) else group
        self.vl.fig.add_annotation(
            x=0,
            y=y,
            text=label,
            font={'size': chartConf["lbl_font_size"], 'family': 'Montserrat', 'color': '#000000'},
            showarrow=False,
            xanchor='left',
            yanchor='bottom',
        )

        y += 1

    self.output(self.vl.fig, chartConf["img_name"])