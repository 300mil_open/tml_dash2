from tml_dash2.utils._util import *
import folium
import branca.colormap as cmp
import geopandas as gpd
from shapely import wkt
from IPython.display import HTML, display
from folium.plugins import MarkerCluster, FeatureGroupSubGroup, Fullscreen
from SecretColors import Palette, utils


def choropleth_map(
        self,
        df,
        val_col,
        cat_col=None,
        group_col=None,
        color_col=None,
        sort_col=None,
        geom_col='geom',
        **kwargs
):
    r"""Choropleth map.

    Parameters
    ----------
    df : DataFrame, panda's dataframe
        Panda's dataframe to be transformed.
    val_col : Column name or list of column names, str
        Column name with numerical values or list of columns with numerical values.
    cat_col : Column name, str
        Column name with categorical values,  if None is passed the val_col column name's are used as categories.
    group_col : Column name, str
        Column name with categorical values,  if None is passed the cat_col column is used as categories.
    color_col : Column name, str
        Column name with numerical or categorical values, If None is passed the val_col column is used as color_col.
    sort_col : Column name, str
        Column name with numerical or categorical values, If None is passed the val_col column is used as sort_col.
    geom_col : Column name, str
        Column name with geometry as text (WKT).
    **kwargs : kwargs
        Plotly and custom keywargs.

    """

    chartConf = updateKwargs(self.defaultKwargs, **kwargs)

    dfChart = prepareDF(self, df=df, val_col=val_col, color_col=color_col, sort_col=sort_col,
                             cat_col=cat_col, group_col=group_col, geom_col=geom_col, **chartConf)

    dfChart['geom_col'] = dfChart['geom_col'].apply(wkt.loads)
    dfChart = gpd.GeoDataFrame(dfChart, geometry='geom_col', crs=chartConf["crs"])
    dfChart.reset_index(inplace=True)
    dfChart = dfChart.explode()

    bbox = dfChart.total_bounds
    m = folium.Map(location=[(bbox[1] + bbox[3]) / 2, (bbox[0] + bbox[2]) / 2], zoom_start=12, tiles=None)
    folium.TileLayer('CartoDB positron', name="Light Map", control=False).add_to(m)

    Fullscreen(
        title='Expand me',
        title_cancel='Exit fullscreen',
        force_separate_button=True
    ).add_to(m)

    def addLayer(m, gdf, color, cat):

        bins = (gdf.shape[0]+1) if chartConf["bins"] is None else (chartConf["bins"]+1)

        cc = utils.hex_to_rgb(color)
        cc = utils.rgb_to_rgb255(max(cc), max(cc), max(cc))
        cc = utils.rgb255_to_hex(cc[0], cc[1], cc[2])
        clrs = [cc, color]
        clrs = makeColors(clrs, bins)
        clrs = clrs[1:]
        clrs = cmp.LinearColormap(
            [clrs[0], clrs[-1]],
            vmin=gdf['val_col'].min(), vmax=gdf['val_col'].max()
        )

        clrs.caption = cat
        clrs.add_to(m)

        g = folium.FeatureGroup(cat, overlay=False)

        p=folium.features.GeoJson(
            gdf,
            style_function=lambda x: {"weight":0.5,
                                    'color':'black',
                                    'fillColor':clrs(x['properties']['val_col']),
                                    'fillOpacity':1.0},
            control=False,
            highlight_function=lambda x: {'fillColor': clrs(x['properties']['val_col']),
                                        'color':'#000000',
                                        'fillOpacity': 0.75,
                                        'weight': 0.75},
            popup=folium.features.GeoJsonPopup(
                fields=['group_col', 'cat_col', 'val_col'],
                sticky=True
            )
        )
        p.add_to(g)
        g.add_to(m)

        return m

    cats = dfChart['cat_col'].unique().tolist()

    for cat, color in zip(cats, self.colors[:len(cats)]):
        dfTemp = dfChart[dfChart["cat_col"]==cat].copy()

        m = addLayer(m, dfTemp, color, cat)

    folium.LayerControl().add_to(m)
    display(m)