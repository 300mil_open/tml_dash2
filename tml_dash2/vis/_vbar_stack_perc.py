import tml_dataclean as tmdc
import plotly.graph_objects as go
import re
from tml_dash2.utils._util import *


def vbar_stack_perc(
        self,
        df,
        val_col,
        cat_col=None,
        group_col=None,
        color_col=None,
        sort_col=None,
        **kwargs
):
    r"""Percentage vertical stacked bar chart.

    Parameters
    ----------
    df : DataFrame, panda's dataframe
        Panda's dataframe to be transformed.
    val_col : Column name or list of column names, str
        Column name with numerical values or list of columns with numerical values.
    cat_col : Column name, str
        Column name with categorical values,  if None is passed the val_col column name's are used as categories.
    group_col : Column name, str
        Column name with categorical values,  if None is passed the cat_col column is used as categories.
    color_col : Column name, str
        Column name with numerical or categorical values, If None is passed the val_col column is used as color_col.
    sort_col : Column name, str
        Column name with numerical or categorical values, If None is passed the val_col column is used as sort_col.
    **kwargs : kwargs
        Plotly and custom keywargs.

    """

    self.vl.reset()

    chartConf = updateKwargs(self.defaultKwargs, **kwargs)
    chartConf["perc_val"] = True
    chartConf["text_threshold"] = None
    #         print(chartConf)

    dfChart = prepareDF(self, df=df, val_col=val_col, color_col=color_col, sort_col=sort_col,
                             cat_col=cat_col, group_col=group_col, **chartConf)

    n = dfChart['cat_col'].unique().shape[0]
    l = self.vl.fig.layout['margin']['l']
    r = self.vl.fig.layout['margin']['r']
    l = l if 'lo_margin_l' not in kwargs else kwargs['lo_margin_l']
    r = r if 'lo_margin_r' not in kwargs else kwargs['lo_margin_r']

    self.vl.update(lo_width=(n * chartConf["size"]) + l + r, **kwargs)

    if self.vl.fig.layout['showlegend']:
        updateLegendSize(self, dfChart['group_col'].unique().shape[0])

    def makeLblCol(lbl, val):
        val = tmdc.clean_decimal(float(val), get_number=False)
        txt = f"{lbl} ({val})"

        if re.match(r"^.*,0\)$", txt):
            txt = txt.replace(',0)', ')')

        return txt

    for c in dfChart['group_col'].unique().tolist():
        dfTemp = dfChart[dfChart['group_col'] == c].copy()

        label = self.legend[c] if (self.legend and c in self.legend) else c
        label = makeLblCol(label, dfTemp['val_abs_col'].sum())

        trace = go.Bar(
            x=dfTemp['cat_col'],
            y=dfTemp['val_col'],
            width=0.5,
            offset=0.25,
            orientation='v',
            marker_color=dfTemp['color_col'],
            name=label
        )

        if chartConf["lbl_show"]:
            trace['text'] = [round(x, 2) for x in dfTemp['val_col']]
            trace['textangle'] = 270
            trace['insidetextfont'] = {'size': chartConf["lbl_font_size"], 'family': 'Montserrat'}
            trace['textposition'] = 'inside'

        self.vl.fig.add_trace(trace)

    for cat in dfChart['cat_col'].unique():
        label = self.legend[cat] if (self.legend and cat in self.legend) else cat
        self.vl.fig.add_annotation(
            x=cat,
            y=0,
            text=label,
            font={'size': chartConf["lbl_font_size"], 'family': 'Montserrat', 'color': '#000000'},
            showarrow=False,
            textangle=270,
            xanchor='right',
            yanchor='bottom',
            xshift=chartConf["lbl_font_size"]
        )

    self.vl.fig.update_layout(barmode='stack')
    self.output(self.vl.fig, chartConf["img_name"])