import plotly.graph_objects as go
from tml_dash2.utils._util import *


def hbar(
        self,
        df,
        val_col,
        cat_col=None,
        color_col=None,
        sort_col=None,
        **kwargs
):
    r"""Horizontal bar chart.

    Parameters
    ----------
    df : DataFrame, panda's dataframe
        Panda's dataframe to be transformed.
    val_col : Column name or list of column names, str
        Column name with numerical values or list of columns with numerical values.
    cat_col : Column name, str
        Column name with categorical values,  if None is passed the val_col column name's are used as categories.
    color_col : Column name, str
        Column name with numerical or categorical values, If None is passed the val_col column is used as color_col.
    sort_col : Column name, str
        Column name with numerical or categorical values, If None is passed the val_col column is used as sort_col.
    **kwargs : kwargs
        Plotly and custom keywargs.

    """

    self.vl.reset()

    chartConf = updateKwargs(self.defaultKwargs, **kwargs)
    chartConf["stack_order"] = None
    chartConf["perc_val"] = False
    chartConf["text_threshold"] = None
    #         print(chartConf)

    dfChart = prepareDF(self, df=df, val_col=val_col, color_col=color_col, sort_col=sort_col,
                             cat_col=cat_col, group_col=None, **chartConf)

    n = dfChart['cat_col'].shape[0]
    t = self.vl.fig.layout['margin']['t']
    b = self.vl.fig.layout['margin']['b']
    t = t if 'lo_margin_t' not in kwargs else kwargs['lo_margin_t']
    b = b if 'lo_margin_b' not in kwargs else kwargs['lo_margin_b']

    self.vl.update(lo_height=(n * chartConf["size"]) + t + b, **kwargs)

    if self.vl.fig.layout['showlegend']:
        updateLegendSize(self, 1)

    name = cat_col if cat_col else dfChart['cat_col'].values[0]

    trace = go.Bar(
        y=dfChart['cat_col'],
        x=dfChart['val_col'],
        width=0.5,
        offset=-0.5,
        orientation='h',
        marker_color=dfChart['color_col'],
        name=name
    )

    self.vl.fig.add_trace(trace)

    for cat in dfChart['cat_col'].unique():
        label = self.legend[cat] if (self.legend and cat in self.legend) else cat
        self.vl.fig.add_annotation(
            x=0,
            y=cat,
            text=label,
            font={'size': chartConf["lbl_font_size"], 'family': 'Montserrat', 'color': '#000000'},
            showarrow=False,
            xanchor='left',
            yanchor='bottom',
            yshift=2
        )

        if chartConf["lbl_show"]:
            w = self.vl.fig.layout['width'] - (
                    self.vl.fig.layout['margin']['r'] + self.vl.fig.layout['margin']['l'])
            x = dfChart[dfChart["cat_col"] == cat]["val_col"].values[0]
            txt = str(round(x, 2))

            threshold = w / dfChart["val_col"].max()

            if (x * threshold) <= ((chartConf["lbl_font_size"]) * len(txt)):
                xanchor = 'left'
                xshift = 2
            else:
                xanchor = 'right'
                xshift = -2

            self.vl.fig.add_annotation(
                x=x,
                y=cat,
                text=txt,
                font={'size': chartConf["lbl_font_size"], 'family': 'Montserrat', 'color': '#000000'},
                showarrow=False,
                textangle=0,
                xanchor=xanchor,
                yanchor='middle',
                yshift=-chartConf["size"] / 4,
                xshift=xshift
            )

    self.output(self.vl.fig, chartConf["img_name"])