import matplotlib.pyplot as plt
from plotly.offline import iplot
from tml_dash2.utils._util import *
from tml_dash2.layout._layout import VisLayout
from ._hbar import hbar
from ._vbar import vbar
from ._hbar_group import hbar_group
from ._vbar_group import vbar_group
from ._hbar_stack import hbar_stack
from ._vbar_stack import vbar_stack
from ._hbar_stack_perc import hbar_stack_perc
from ._vbar_stack_perc import vbar_stack_perc
from ._donut import donut
from ._waffle import waffle
# from ._choropleth_map import choropleth_map


class Vis:
    r"""Main Visualization object from which all plots will be generated

    Example
    --------
    import tml_dash2 as tmld2

    # Defines a Vis object with a folder_out to save the images and a file used as a legend dictionary, and defyning with and legend visibility for the charts
    vis = Vis(
        colors=['#ffffb2', '#fd8d3c', '#bd0026'],
        folder_out = './Downloads',
        legend = './leyenda.xls',
        lo_width = 800,
        lo_legend_visible=True
    )

    # Defines a default Vis object just for visualization
    vis = Vis()
    """

    def __init__(self, colorscale=['#000000'], color_single=None, folder_out=None, img_format='png', legend=None, **kwargs):
        r"""Main Vis object.

        Parameters
        ----------
        colorscale : Color or list of colors, str or list
            Single color or list of colors in string format.
        color_single : Color, str
            Color to be used when charts use a single color, if None is passed uses the first color of the colorscale.
        folder_out : Folder path, str
            Folder path where images will be saved, if folder doesn't exist it is created and if None images will be just shown and not automaticaly saved.
        img_format : Image format, str
            Image format, PNG by default.
        legend : Excel file path or dict, str or dict
            File or dict with labels and their correspondences to be used in legends.
        **kwargs : kwargs
            Plotly and custom keywargs.

        """

        self.colors = colorscale
        self.color_single = colorscale[0] if color_single is None else color_single
        kwargs["colors"] = self.colors
        kwargs["color_single"] = self.color_single
        self.vl = VisLayout(**kwargs)

        self.folder_out = folder_out

        if folder_out:
            if os.path.isdir(folder_out) is False:
                os.mkdir(folder_out)
                print(f"{folder_out} directory created.")

        self.img_format = img_format

        self.legend = make_legend(legend)

        self.defaultKwargs = defaultVals(**kwargs)


    def output(self, fig, img_name, fig_type='plotly'):
        r"""Controls the output of each chart either it saves the images or just show them.

        Parameters
        ----------
        fig : Plotly Fig object, plotly object
            Vis Fig object to be used as a canvas to the chart.
        img_name : File name, str
            Name of the file to the image to be saved into.
        fig_type : Figure type, str
            Figure type to run the show or save function based on. Accepts matplotlib, plotly (seaborn to be added);

        """

        if fig_type == 'plotly':
            if self.folder_out is None:
                iplot(fig)
                return

            if self.folder_out and os.path.isdir(self.folder_out):
                fig.write_image(f"{self.folder_out}/{img_name}.{self.img_format}", format=self.img_format)

            iplot(fig)

        elif fig_type == 'matplotlib':
            if self.folder_out is None:
                plt.show()
                return

            if self.folder_out and os.path.isdir(self.folder_out):
                plt.savefig(f"{self.folder_out}/{img_name}.{self.img_format}", bbox_inches='tight', facecolor=None,
                            edgecolor=None)

            plt.show()


    def hbar(self, df, val_col, cat_col=None, color_col=None, sort_col=None, **kwargs):
        hbar(self, df, val_col, cat_col=cat_col, color_col=color_col, sort_col=sort_col, **kwargs)


    def vbar(self, df, val_col, cat_col=None, color_col=None, sort_col=None, **kwargs):
        vbar(self, df, val_col, cat_col=cat_col, color_col=color_col, sort_col=sort_col, **kwargs)


    def hbar_group(self, df, val_col, cat_col=None, group_col=None, color_col=None, sort_col=None, **kwargs):
        hbar_group(self, df, val_col, cat_col=cat_col, group_col=group_col, color_col=color_col, sort_col=sort_col, **kwargs)


    def vbar_group(self, df, val_col, cat_col=None, group_col=None, color_col=None, sort_col=None, **kwargs):
        vbar_group(self, df, val_col, cat_col=cat_col, group_col=group_col, color_col=color_col, sort_col=sort_col, **kwargs)


    def hbar_stack( self, df, val_col, cat_col=None, group_col=None, color_col=None, sort_col=None, **kwargs):
        hbar_stack(self, df, val_col, cat_col=cat_col, group_col=group_col, color_col=color_col, sort_col=sort_col, **kwargs)


    def vbar_stack( self, df, val_col, cat_col=None, group_col=None, color_col=None, sort_col=None, **kwargs):
        vbar_stack(self, df, val_col, cat_col=cat_col, group_col=group_col, color_col=color_col, sort_col=sort_col, **kwargs)


    def hbar_stack_perc(self, df, val_col,cat_col=None, group_col=None, color_col=None, sort_col=None, **kwargs):
        hbar_stack_perc(self, df, val_col, cat_col=cat_col, group_col=group_col, color_col=color_col, sort_col=sort_col, **kwargs)


    def vbar_stack_perc(self, df, val_col,cat_col=None, group_col=None, color_col=None, sort_col=None, **kwargs):
        vbar_stack_perc(self, df, val_col, cat_col=cat_col, group_col=group_col, color_col=color_col, sort_col=sort_col, **kwargs)


    def donut(self, df, val_col, cat_col = None, color_col = None, sort_col = None, **kwargs):
        donut(self, df, val_col, cat_col=cat_col, color_col=color_col, sort_col=sort_col, **kwargs)


    def waffle(self, df, val_col = None, cat_col = None, color_col = None, sort_col = None, **kwargs):
        waffle(self, df, val_col=val_col, cat_col=cat_col, color_col=color_col, sort_col=sort_col, **kwargs)


    # def choropleth_map(self, df, val_col, cat_col=None, group_col=None, color_col=None, sort_col=None, geom_col='geom', **kwargs ):
    #     return choropleth_map(self, df, val_col, cat_col=cat_col, group_col=group_col, color_col=color_col, sort_col=sort_col, geom_col=geom_col, **kwargs)