import pandas as pd
import plotly.graph_objects as go
from tml_dash2.utils._util import *


def hbar_stack(
        self,
        df,
        val_col,
        cat_col=None,
        group_col=None,
        color_col=None,
        sort_col=None,
        **kwargs
):
    r"""Absolute horizontal stacked bar chart.

    Parameters
    ----------
    df : DataFrame, panda's dataframe
        Panda's dataframe to be transformed.
    val_col : Column name or list of column names, str
        Column name with numerical values or list of columns with numerical values.
    cat_col : Column name, str
        Column name with categorical values,  if None is passed the val_col column name's are used as categories.
    group_col : Column name, str
        Column name with categorical values,  if None is passed the cat_col column is used as categories.
    color_col : Column name, str
        Column name with numerical or categorical values, If None is passed the val_col column is used as color_col.
    sort_col : Column name, str
        Column name with numerical or categorical values, If None is passed the val_col column is used as sort_col.
    **kwargs : kwargs
        Plotly and custom keywargs.

    """

    self.vl.reset()

    chartConf = updateKwargs(self.defaultKwargs, **kwargs)
    chartConf["perc_val"] = False
    chartConf["text_threshold"] = None
    #         print(chartConf)

    dfChart = prepareDF(self, df=df, val_col=val_col, color_col=color_col, sort_col=sort_col,
                             cat_col=cat_col, group_col=group_col, **chartConf)

    n = dfChart['cat_col'].unique().shape[0]
    t = self.vl.fig.layout['margin']['t']
    b = self.vl.fig.layout['margin']['b']
    t = t if 'lo_margin_t' not in kwargs else kwargs['lo_margin_t']
    b = b if 'lo_margin_b' not in kwargs else kwargs['lo_margin_b']

    self.vl.update(lo_height=(n * chartConf["size"]) + t + b, **kwargs)

    if self.vl.fig.layout['showlegend']:
        updateLegendSize(self, dfChart['group_col'].unique().shape[0])

    if dfChart['val_col'].min() < 0:
        d_neg = dfChart[dfChart['val_col'] < 0].copy()
        d_pos = dfChart[dfChart['val_col'] >= 0].copy()

        d_neg['val_col_neg'] = -d_neg['val_col']
        d_neg.sort_values('val_col_neg', ascending=False, inplace=True)
        d_neg.reset_index(inplace=True)
        d_neg['idx'] = d_neg.index
        d_neg['base'] = d_neg.apply(
            lambda row: d_neg['val_col'].sum() + abs(d_neg[d_neg.index <= row['idx']]['val_col'].sum()), axis=1)
        d_neg.drop(['idx'], axis=1, inplace=True)

        d_pos['val_col_neg'] = d_pos['val_col']
        d_pos.sort_values('val_col_neg', ascending=True, inplace=True)
        d_pos.reset_index(inplace=True)
        d_pos['idx'] = d_pos.index
        d_pos['base'] = d_pos.apply(lambda row: abs(d_pos[d_pos.index < row['idx']]['val_col'].sum()), axis=1)
        d_pos.drop(['idx'], axis=1, inplace=True)

        dfChart = pd.concat([d_neg, d_pos], ignore_index=True)

    for c in dfChart['group_col'].unique().tolist():
        label = self.legend[c] if (self.legend and c in self.legend) else c
        dfTemp = dfChart[dfChart['group_col'] == c].copy()

        trace = go.Bar(
            y=dfTemp['cat_col'],
            x=dfTemp['val_col'],
            width=0.5,
            offset=-0.5,
            orientation='h',
            marker_color=dfTemp['color_col'],
            name=label
        )

        if chartConf["lbl_show"]:
            trace['text'] = [round(x, 2) for x in dfTemp['val_col']]
            trace['textangle'] = 0
            trace['insidetextfont'] = {'size': chartConf["lbl_font_size"], 'family': 'Montserrat'}
            trace['textposition'] = 'inside'

        if dfChart['val_col'].min() < 0:
            trace['base'] = dfTemp['base']

        self.vl.fig.add_trace(trace)

    for cat in dfChart['cat_col'].unique():
        x = 0 if dfChart['val_col'].min() >= 0 else dfChart[dfChart['val_col'] < 0]['val_col'].sum()
        label = self.legend[cat] if (self.legend and cat in self.legend) else cat
        self.vl.fig.add_annotation(
            x=x,
            y=cat,
            text=label,
            font={'size': chartConf["lbl_font_size"], 'family': 'Montserrat', 'color': '#000000'},
            showarrow=False,
            xanchor='left',
            yanchor='bottom',
            yshift=2
        )

    if dfChart['val_col'].min() >= 0:
        self.vl.fig.update_layout(barmode='stack')
    else:
        self.vl.fig.update_layout(barmode='overlay')

    self.output(self.vl.fig, chartConf["img_name"])