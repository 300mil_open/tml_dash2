import plotly.graph_objects as go
# import os
import re
from tml_dash2.utils._util import *


class VisLayout:
    r"""VisLayout object, used as a nested object in the Vis object

    Example
    --------
    check kwargDict for possible kwargs (constantly updated).

    """

    def __init__(self, **kwargs):
        self.fig = go.Figure()
        self.cartesian_layout = self.cartesian(**kwargs)

        self.cartesian_template = go.layout.Template()
        self.cartesian_template.layout = self.cartesian_layout


    def getMPLFont(self):
        r"""Gets the path of Montserrat font for Matplotlib charts.

        Returns
        -------
        Font path
            Montserrat font path in thecomputer.

        """
        for dirpath, dirnames, filenames in os.walk("/home/"):
            if re.match(r"/home/.*/share/fonts", dirpath):
                font_folder = dirpath

        font = f"{font_folder}/montserrat_normal_400.ttf"

        return font


    def cartesian(self, **kwargs):
        r"""Generates a X/Y Plotly layout figure.

        Parameters
        ----------
        **kwargs : kwargs
            Plotly and custom keywargs.

        Returns
        -------
        Plotly Layout object
            Plotly layout object updated by the given kwargs.

        """

        updateDict = defaultVals(**kwargs)

        for i in updateDict.items():
            updt = getPlotlyKwarg(i[0], i[1])

            if updt:
                if re.match(r"^lo\_.*$", i[0]):
                    self.fig.update_layout(updt)
                elif re.match(r"^x\_.*$", i[0]):
                    self.fig.update_xaxes(updt)
                elif re.match(r"^y\_.*$", i[0]):
                    self.fig.update_yaxes(updt)

        return self.fig.layout


    def reset(self):
        r"""Resets the Plotly Figure to its initial state.

        """

        self.fig['data'] = []
        self.fig['layout'] = self.cartesian_layout


    def update(self, **kwargs):
        r"""Updates the Plotly Layout using the given kwargs.

        Parameters
        ----------
        **kwargs : kwargs
            Plotly and custom keywargs.

        """

        for i in kwargs.items():
            updt = getPlotlyKwarg(i[0], i[1])

            if updt:
                if re.match(r"^lo\_.*$", i[0]):
                    self.fig.update_layout(updt)
                elif re.match(r"^x\_.*$", i[0]):
                    self.fig.update_xaxes(updt)
                elif re.match(r"^y\_.*$", i[0]):
                    self.fig.update_yaxes(updt)