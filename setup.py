from setuptools import setup, find_packages

setup(
    name='tml_dash2',
    version='1.4.7',
    packages=find_packages(),
    include_package_data=True,
    url='https://gitlab.com/300mil/tml_dash2',
    license='',
    author='Andre Resende',
    author_email='andre@300000kms.net',
    description='Transversal package for chart building in Python',
    install_requires=[
        'SQLAlchemy >= 1.3.22',
        'tml_dataclean >= 1.1.10',
        'pandas >= 1.2.0',
        'numpy >= 1.17.4',
        'tml_pandas >= 0.0.2',
        'matplotlib >= 3.1.2',
        'plotly >= 4.4.1',
        'scipy >= 1.6.0',
        'ipython >= 7.21.0',
        'pywaffle >= 0.6.1',
        'SecretColors >= 1.2.4'
    ]
)



